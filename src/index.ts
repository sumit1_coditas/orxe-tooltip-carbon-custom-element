import "./components/carbon-element";

import { customElement, LitElement, html } from "lit-element";

@customElement("root-el")
export class RootElement extends LitElement {
  render() {
    return html`
      <style>
      orxe-tooltip {
        position:absolute;
        left: 200px;
      }
      </style>
        <orxe-tooltip open="false" direction="bottom" content="This is some tooltip text. This box shows the maximum amount of text that should appear inside. If more room is
      needed please use a modal instead."></orxe-tooltip>
    `;
  }
  createRenderRoot() {
    return this;
  }
}
